# 1. How-to edit /etc/hosts file on Windows

###### For Windows 10 and 8

1. Press the Windows key.
2. Type Notepad in the search field.
3. In the search results, right-click Notepad and select Run as administrator.
4. From Notepad, open the following file: c:\Windows\System32\Drivers\etc\hosts
5. Make the necessary changes to the file.
6. Click File > Save to save your changes.

###### For Windows 7 and Vista

1. Click Start > All Programs > Accessories.
2. Right-click Notepad and select Run as administrator.
3. Click Continue on the Windows needs your permission UAC window.
4. When Notepad opens, click File > Open.
5. In the File name field, type C:\Windows\System32\Drivers\etc\hosts.
6. Click Open.
7. Make the necessary changes to the file.
8. Click File > Save to save your changes.

###### For Windows NT, Windows 2000, and Windows XP

1. Click Start > All Programs > Accessories > Notepad.
2. Click File > Open.
3. In the File name field, type C:\Windows\System32\Drivers\etc\hosts.
4. Click Open.
5. Make the necessary changes to the file.
6. Click File > Save to save your changes.